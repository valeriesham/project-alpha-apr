from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    form = TaskForm(request.POST)

    if form.is_valid():
        # form.instance.owner = request.user
        form.save()
        return redirect("list_projects")

    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def list_tasks(request):
    list = Task.objects.filter(assignee=request.user)
    context = {"tasks_list": list}
    return render(request, "tasks/list_tasks.html", context)
